namespace KidsGenius.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _04042018 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.UserCourses", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.UserCourses", "UserId", "dbo.Users");
            DropIndex("dbo.UserCourses", new[] { "UserId" });
            DropIndex("dbo.UserCourses", new[] { "Role_Id" });
            //DropTable("dbo.UserCourses");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.UserCourses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        CourseId = c.Int(nullable: false),
                        CourseStartDate = c.DateTime(nullable: false),
                        Role_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.UserCourses", "Role_Id");
            CreateIndex("dbo.UserCourses", "UserId");
            AddForeignKey("dbo.UserCourses", "UserId", "dbo.Users", "Id", cascadeDelete: true);
            AddForeignKey("dbo.UserCourses", "Role_Id", "dbo.Roles", "Id");
        }
    }
}
