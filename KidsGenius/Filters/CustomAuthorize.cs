﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KidsGenius.Filters
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        private string[] allowedRoles = new string[] { };

        public CustomAuthorize()
        { }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated && Role(filterContext.HttpContext))
            {
                base.OnAuthorization(filterContext);
            }
            else if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                filterContext.Result = new RedirectResult("~/MyRoom/MyCourses");
            }
            else
            {
                filterContext.Result = new RedirectResult("~/Account/Login");
            }
        }

        //protected override bool AuthorizeCore(HttpContextBase httpContext)
        //{
        //    if (!String.IsNullOrEmpty(base.Roles))
        //    {
        //        allowedRoles = base.Roles.Split(new char[] { ',' });
        //        for (int i = 0; i < allowedRoles.Length; i++)
        //        {
        //            allowedRoles[i] = allowedRoles[i].Trim();
        //        }
        //    }

        //    //if (Role(httpContext))
        //    //{
        //    //    return httpContext.Request.IsAuthenticated;
        //    //}
        //    //else
        //    //{
        //    //    return !httpContext.Request.IsAuthenticated;
        //    //}

        //    return httpContext.Request.IsAuthenticated && Role(httpContext);
        //}

        private bool Role(HttpContextBase httpContext)
        {
            if (!String.IsNullOrEmpty(Roles))
            {
                allowedRoles = Roles.Split(new char[] { ',' });
                for (int i = 0; i < allowedRoles.Length; i++)
                {
                    allowedRoles[i] = allowedRoles[i].Trim();
                }
            }

            if (allowedRoles.Length > 0)
            {
                for (int i = 0; i < allowedRoles.Length; i++)
                {
                    if (httpContext.User.IsInRole(allowedRoles[i]))
                        return true;
                }
                return false;
            }
            return true;
        }
    }
}