﻿window.onscroll = function () { myFunction() };

var stickyHeader = document.getElementById("sticky-header");
var contentStart = document.getElementById("courseLeader");
var sticky = contentStart.offsetTop;

function myFunction() {
    if (window.pageYOffset >= sticky) {
        stickyHeader.classList.add("sticky");
    } else {
        stickyHeader.classList.remove("sticky");
    }
}