﻿using KidsGenius.DAL;
using KidsGenius.Filters;
using KidsGenius.Models;
using System.Web;
using System.Web.Mvc;

namespace KidsGenius.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        CourseContext db = new CourseContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View(db.Courses);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Course course, HttpPostedFileBase postedFile = null)
        {
            if (postedFile != null)
            {
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                postedFile.SaveAs(Server.MapPath("~/Content/images/" + fileName));
                course.MainPicture = "../../Content/images/" + fileName;
                SaveCourse(course);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Course course = db.Courses.Find(id);

            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        [HttpPost]
        public ActionResult Edit(Course course, HttpPostedFileBase postedFile = null)
        {
            if (postedFile != null)
            {
                string fileName = System.IO.Path.GetFileName(postedFile.FileName);
                postedFile.SaveAs(Server.MapPath("~/Content/images/" + fileName));
                course.MainPicture = "../../Content/images/" + fileName;
                SaveCourse(course);
            }
            else
            {
                Course currentCourse = db.Courses.Find(course.Id);
                course.MainPicture = currentCourse.MainPicture;
                SaveCourse(course);
            }
            
            return RedirectToAction("Edit/" + course.Id);
        }
        
        [HttpGet, ActionName("Delete")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            db.Courses.Remove(course);
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        public void SaveCourse(Course course)
        {
            if (course.Id == 0)
                db.Courses.Add(course);
            else
            {
                Course dbEntry = db.Courses.Find(course.Id);
                if (dbEntry != null)
                {
                    dbEntry.Name = course.Name;
                    dbEntry.MainPicture = course.MainPicture;
                    dbEntry.Description = course.Description;
                    dbEntry.StyleColor = course.StyleColor;
                }
            }
            db.SaveChanges();
        }


    }
}