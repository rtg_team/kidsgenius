﻿using KidsGenius.DAL;
using KidsGenius.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace KidsGenius.Controllers
{
    public class CoursesController : Controller
    {
        CourseContext db = new CourseContext();

        public ActionResult CoursesList()
        {
            IEnumerable<Course> courses = db.Courses;

            ViewBag.Courses = courses;

            return View();
        }

        public ActionResult Course(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            Course course = db.Courses.Find(id);

            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        public ActionResult BuyCourse()
        {


            return View();
        }
    }
}