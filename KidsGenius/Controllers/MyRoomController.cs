﻿using KidsGenius.DAL;
using KidsGenius.Filters;
using KidsGenius.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace KidsGenius.Controllers
{
    [CustomAuthorize]
    public class MyRoomController : Controller
    {
        CourseContext courseDb = new CourseContext();
        
        public ActionResult MyCourses()
        {
            User user = courseDb.Users.FirstOrDefault(u => u.Email == User.Identity.Name);

            List<Course> userCourses = new List<Course>();

            foreach (UserCourses userCourse in courseDb.UsersCourses)
            {
                if (userCourse.UserId == user.Id)
                {
                    userCourses.Add(courseDb.Courses.FirstOrDefault(c => c.Id == userCourse.CourseId));
                }
            }

            if (userCourses == null)
            {

            }

            ViewBag.UserCourses = userCourses;

            return View();
        }

        public ActionResult Course(int? id)
        {


            return View();
        }

    }
}