﻿using System.ComponentModel.DataAnnotations;

namespace KidsGenius.Models
{
    public class Course
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Course name")]
        public string Name { get; set; }

        [Display(Name = "Main picture")]
        public string MainPicture { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Header color")]
        public string StyleColor { get; set; }
    }
}