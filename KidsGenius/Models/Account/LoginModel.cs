﻿using System.ComponentModel.DataAnnotations;

namespace KidsGenius.Models.Account
{
    public class LoginModel
    {
        [Required]
        public string Email { get; set; }        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        public bool IsRemembered { get; set; }
    }
}