﻿using KidsGenius.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace KidsGenius.DAL
{
    public class CourseContext : DbContext
    {
        public CourseContext(): base("CoursesDbConnection")
        { }

        public DbSet<Course> Courses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserCourses> UsersCourses { get; set; } 
    }

    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserSecondname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public int? RoleId { get; set; }
        public Role Role { get; set; }
    }

    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class UserCourses
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CourseId { get; set; }
        public DateTime CourseStartDate { get; set; }
    }

    public class CourseVideos {

    }
}