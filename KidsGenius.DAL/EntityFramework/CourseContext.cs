﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KidsGenius.DAL.Models;

namespace KidsGenius.DAL.EntityFramework
{
    public class CourseContext : DbContext
    {
        public CourseContext() : base("CoursesDbConnection")
        { }

        public DbSet<Course> Courses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserCourses> UsersCourses { get; set; }
    }
}
